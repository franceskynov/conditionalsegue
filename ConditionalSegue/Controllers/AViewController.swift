//
//  AViewController.swift
//  ConditionalSegue
//
//  Created by Dev on 10/16/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class AViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func noAct(_ sender: UIButton) {
        performSegue(withIdentifier: "no", sender: self)
    }
    
    @IBAction func yesAct(_ sender: UIButton) {
        performSegue(withIdentifier: "yes", sender: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
